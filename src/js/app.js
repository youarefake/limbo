//= ../libs/jquery/dist/jquery.min.js
//= ../libs/slick-carousel/slick/slick.min.js

window.onload = function() {
  $("#catalogSlider").slick({
    slidesToShow: 3,
    dots: true,
    arrows: false,
    slidesToScroll: 1,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 2000,
    useTransform: true,
    cssEase: "cubic-bezier(0.645, 0.045, 0.355, 1.000)",
    customPaging: function(slider, i) {
      return '<div class="slider-dot"></div>';
    },
    responsive: [
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $("#catalogSlider-2").slick({
    slidesToShow: 3,
    dots: true,
    arrows: false,
    slidesToScroll: 1,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 2000,
    useTransform: true,
    cssEase: "cubic-bezier(0.645, 0.045, 0.355, 1.000)",
    customPaging: function(slider, i) {
      return '<div class="slider-dot"></div>';
    },
    responsive: [
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  let arrowSlider = document.querySelector("#arrowSlider");
  if (arrowSlider) {
    var imageCount = $("#arrowSlider .slider-item").length;
    $("#arrowSlider").slick({
      prevArrow: $(".arrows-wrapper .arrow-prev"), // change arrow for custom
      nextArrow: $(".arrows-wrapper .arrow-next"), // change arrow for custom
      slidesToShow: Math.min(3, imageCount - 1),
      dots: false,
      arrows: true,
      infinite: true,
      slidesToScroll: 1,
      speed: 1000,
      autoplay: true,
      autoplaySpeed: 2000,
      cssEase: "cubic-bezier(0.645, 0.045, 0.355, 1.000)",
      responsive: [
        {
          breakpoint: 901,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 641,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  }

  let arrowSlider2 = document.querySelector("#arrowSlider-2");
  if (arrowSlider2) {
    var imageCount = $("#arrowSlider-2 .slider-item").length;
    $("#arrowSlider-2").slick({
      prevArrow: $(".arrows-wrapper .arrow-prev"), // change arrow for custom
      nextArrow: $(".arrows-wrapper .arrow-next"), // change arrow for custom
      slidesToShow: Math.min(3, imageCount - 1),
      dots: false,
      arrows: true,
      infinite: true,
      slidesToScroll: 1,
      speed: 1000,
      autoplay: true,
      autoplaySpeed: 2000,
      cssEase: "cubic-bezier(0.645, 0.045, 0.355, 1.000)",
      responsive: [
        {
          breakpoint: 901,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 641,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  }

  $("#verticalSLider").slick({
    prevArrow: $(".vertical-arrows .arrow-prev"), // change arrow for custom
    nextArrow: $(".vertical-arrows .arrow-next"), // change arrow for custom
    slidesToShow: 3,
    dots: false,
    arrows: true,
    swipe: false,
    touchMove: false,
    draggable: false,
    vertical: true,
    speed: 1000,
    useTransform: true,
    adaptiveHeight: true,
    cssEase: "cubic-bezier(0.645, 0.045, 0.355, 1.000)",
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          vertical: false,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 481,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  let burgerBtn = document.querySelector("#burgerBtn");
  if (burgerBtn) {
    burgerBtn.addEventListener("click", function() {
      this.classList.toggle("btn-opened");
      this.parentNode
        .querySelector(".burger-nav")
        .classList.toggle("burger-nav-opened");
    });
  }

  let btnOpen = document.querySelector("#btnOpen");
  if (btnOpen) {
    btnOpen.addEventListener("click", function() {
      this.parentNode
        .querySelector(".news-filter")
        .classList.add("filter-open");
      this.style.display = "none";
    });
  }

  let btnClose = document.querySelector("#btnClose");
  if (btnClose) {
    btnClose.addEventListener("click", function() {
      this.parentNode.parentNode.classList.remove("filter-open");
      btnOpen.style.display = "block";
    });
  }
  let langSwitch = document.querySelector("#langSwitch");
  if (langSwitch) {
    langSwitch.querySelector("span").addEventListener("click", function() {
      langSwitch.classList.toggle("opened");
    });
  }
};
